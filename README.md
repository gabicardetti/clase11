Los enpoints para consultar las dos vistas son


Para crear un producto
http://localhost:8080/crear

Para listar los productos
http://localhost:8080/listar

Dejo la colección del postman para probar los otros enpoints



## ¿Cuál de los tres motores de plantillas prefiero para mi proyecto?   
 Con lo poco que use cada uno me quedaría con Handlebars, más que nada por la sintaxis de las cosas.

 Handlebars fue de las que más rápido pude entender como funcionaba para poder implementarlo y la verdad que no veo 
 la ventaja de por ejemplo en pug usar una sintaxis tan diferente al html, yo que encima no soy tanto del frontend prefiero
 lo más parecido al html.      
 En un proyecto que tuve me toco usar **markup-js** y me gusto porque comparte un poco con Handlebars la sintaxis y es html con un poquito mas de power,
 son mas simples a mi modo de ver.


## El proyecto tiene 3 branchs:     
    master -- es el proyecto de la clase 10 implementado con Handlebars      
    pug-implementation -- en esta rama está la implementación con pug       
    ejs-implementation -- en esta rama está la implementación con ejs      

Elegí hacerlo con branchs más que nada porque si en un futuro necesito hacer algo con un motor de plantillas en particular
me parece más fácil poder meterse a una branch, seguir trabajando y commiteando para mantener cada trabajo con el motor de plantillas por diferente lugar.

Aunque también me hubiera gustado armar todo en un solo branch y poder decidir por una variable de entorno con cuál motor de plantillas correr el proyecto.