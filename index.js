import express from "express";
import exphbs from 'express-handlebars';
import ProductRoutes from "./product/productRoutes.js"
import { getAllProducts } from "./product/productService.js"
const app = express();
const port = 8080;
const baseUrl = "/api";

app.set('view engine', 'ejs');


app.use(express.json())
// app.use(express.static('public'))

app.get("/", (req, res) => {
  res.render('createProduct');
})

app.get("/listar", (req, res) => {
  const products = getAllProducts();
  res.render('listProducts', {
    products
  });
});

app.use(baseUrl, ProductRoutes);

app.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});
